﻿using afc.id.Base;
using afc.id.Utility;
using Microsoft.AspNetCore.Mvc;

namespace afc.id.Controllers
{
    [Route("api/auth")]
    [ApiController]
    public class AuthorizationController : ControllerBase
    {
        [HttpGet]
        public ActionResult<GenericResponse<bool>> Get([FromHeader(Name = "Authorization")] string authorization)
        {
            var token = Utility.Util.SplitAuthorizationHeader(authorization);
            var returnObject = new GenericResponse<bool>(Utility.TokenManagement.ValidateToken(token));
            return returnObject;
        }
    
    }
}