﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using afc.id.Base;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using RestSharp;
using afc.id.Utility;

namespace afc.id.Controllers
{
    [Route("api/connect")]
    [ApiController]
    public class TokenController : ControllerBase
    {
        private IConfiguration _configuration;
        public TokenController(IConfiguration Configuration)
        {
            _configuration = Configuration;
        }
        [Route("token")]
        [HttpGet]
        public ActionResult<GenericResponse<Token>> Get([FromHeader(Name = "Authorization")] string authorizationHeader)
        {
            var language = Request.GetTypedHeaders().AcceptLanguage.FirstOrDefault()?.Value.ToString();

            var isUserValid = ValidateUser(authorizationHeader, language);
            if (!isUserValid.Value)
            {
                return new GenericResponse<Token>(null, isUserValid.Results);
            }
            return TokenManagement.GenerateToken(authorizationHeader);
        }
        private static GenericResponse<bool> ValidateUser(string authorizationHeader, string lang)
        {
            var userToken = Util.B64_decodeString(Util.SplitAuthorizationHeader(authorizationHeader));
            var userName = userToken.Split(':')[0];
            var userPassword = userToken.Split(':')[1];
            return ValidateUser(userName, userPassword, lang);
        }
        private static GenericResponse<bool> ValidateUser(string username, string pass, string lang)
        {
            var returnObject = new GenericResponse<bool>(false);
            var client = new RestClient("http://localhost/AFC.AWA.WebApi/customer/validate");
            var request = new RestRequest(Method.POST);
            request.AddHeader("Accept-Language", lang);
            request.AddHeader("Content-Type", "application/json");
            request.AddParameter("customer", $"{{\"Email\":\"{username}\"}}", ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var returnResponse = JsonConvert.DeserializeObject<GenericResponse<bool>>(response.Content);
                return returnResponse;
            }
            return returnObject;
        }

    }
}