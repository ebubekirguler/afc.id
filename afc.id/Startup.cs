﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using afc.id.Middleware;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace afc.id
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseStaticFiles();
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            //Add our new middleware to the pipeline
            app.UseMiddleware<RequestResponseLoggingMiddleware>();
            app.UseHttpsRedirection();
            app.UseMvc();

            Utility.TokenManagement.CertificatePath = Configuration.GetSection("Certificate").GetSection("Path").Value;
            Utility.TokenManagement.CertificatePassword = Configuration.GetSection("Certificate").GetSection("Password").Value;
            Utility.TokenManagement.Audience = Configuration.GetSection("Token").GetSection("Audience").Value;
            Utility.TokenManagement.Issuer = Configuration.GetSection("Token").GetSection("Issuer").Value;
            Utility.TokenManagement.ExpireSeconds = int.Parse(Configuration.GetSection("Token").GetSection("ExpireSeconds").Value);
            Utility.TokenManagement.TokenType = Configuration.GetSection("Token").GetSection("Type").Value;

        }
    }
}
