﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace afc.id.Utility
{
    public class Util
    {
        public static string SplitAuthorizationHeader(string authorization)
        {
            return authorization.Split(' ')[1];
        }
        public static string B64_encodeString(String rawString)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(rawString);
            return System.Convert.ToBase64String(plainTextBytes);
        }
        public static string B64_decodeString(String encodedString)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(encodedString);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }
    }
}
