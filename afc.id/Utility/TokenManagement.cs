﻿using afc.id.Base;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;

namespace afc.id.Utility
{
    public class TokenManagement
    {
        #region Props   
        public static string CertificatePath { get; set; }
        public static string CertificatePassword { get; set; }
        public static string Issuer { get; set; }
        public static string Audience { get; set; }
        public static int ExpireSeconds { get; set; }
        public static string TokenType { get; set; }
        #endregion
        public static GenericResponse<Token> GenerateToken(string authorizationHeader)
        {

            var tokenHandler = new JwtSecurityTokenHandler();
            var certificate = new X509Certificate2(CertificatePath, CertificatePassword);
            var securityKey = new X509SecurityKey(certificate);

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(),
                Issuer = Issuer,
                IssuedAt = DateTime.Now,
                Audience = Audience,
                Expires = DateTime.Now.AddSeconds(ExpireSeconds),
                SigningCredentials = new SigningCredentials(
                    securityKey,
                    SecurityAlgorithms.RsaSha256Signature)
            };

            var token = tokenHandler.CreateToken(tokenDescriptor);

            var returnObject = new GenericResponse<Token>(PrepareTokenResponse(tokenHandler.WriteToken(token)));
            return returnObject;
        }
        public static bool ValidateToken(string token)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var certificate = new X509Certificate2(CertificatePath, CertificatePassword);
            var securityKey = new X509SecurityKey(certificate);

            var validationParameters = new TokenValidationParameters
            {
                ValidAudience = Audience,
                ValidIssuer = Issuer,
                IssuerSigningKey = securityKey
            };
            try
            {
                var principal = tokenHandler.ValidateToken(token, validationParameters, out SecurityToken securityToken);
                if (principal == null)
                    return false;
                if (securityToken == null)
                    return false;
                return true;

            }
            catch (SecurityTokenInvalidLifetimeException e)
            {
                return false;
            }
            catch (Exception e)
            {
                return false;
            }
        }


        private static Token PrepareTokenResponse(string accessToken)
        {
            var expires_in = ExpireSeconds;
            var token_type = TokenType;
            return new Token()
            {
                access_token = accessToken,
                expires_in = expires_in,
                token_type = token_type
            };
        }
    }
}
