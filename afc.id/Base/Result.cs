﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace afc.id.Base
{
    public class Result
    {
        public string ErrorCode { get; set; }
        public string ErrorMessage { get; set; }
        public bool IsFriendly { get; set; }
    }
}
